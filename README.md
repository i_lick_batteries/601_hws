# 10-601 Homework Repository #

Contains code for SVM, Logistic Regression and Neural Networks. Written for the Fall 2014 instance of 10-601 Machine Learning at CMU.

## Directory structure

Folder  | Description
------  | -----------
AE      | Autoencoder code. Somewhat messy and probably not useful for use in a homework.
Kernels | Contains code to run Kernelized versions of LR or SVM. Both are trained using gradient methods (either SGD or L-BFGS).
LR      | Logistic Regression code. Can be used to train and test a LR classifier (or SVM).
NN      | Neural Network code.
data    | Contains some datasets used by the different classifiers.
shared  | Some helper functions used all over the code.